# -*- coding: utf-8 -*-
"""
Created on Fri Jul 20 10:17:13 2018

@author: teo
"""

import nltk
from nltk.probability import  FreqDist
import nltk.classify.util
from nltk.corpus import movie_reviews, TaggedCorpusReader, stopwords
from nltk.classify import NaiveBayesClassifier
from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk import word_tokenize
from nltk.tag import pos_tag_sents
import string
from nltk import bigrams
import collections
from collections import Counter
from nltk.tokenize import SpaceTokenizer
from nltk.corpus.reader import ChunkedCorpusReader
import random
from sklearn import cross_validation
import numpy as np
# =============================================================================
#                       Week 3
# =============================================================================

#import the movie reviews dataset
movie_reviews=nltk.corpus.movie_reviews

#Separate the storage to positive and negative review files categories
neg_files=movie_reviews.fileids('neg')
pos_files=movie_reviews.fileids('pos')
print ('The movie reviews storage contains:',len(movie_reviews.fileids()),'files')
print ('The categories of the files are',movie_reviews.categories())
print ('The negative reviews storage contains:',len(neg_files),'files')
print ('The positive reviews storage contains:',len(pos_files),'files')

#store the neg/pos files content to lists=>
neg_text = movie_reviews.raw(categories='neg')
pos_text = movie_reviews.raw(categories='pos')

#tokenize pos/neg lists
neg_tokenized_text = word_tokenize(neg_text)
pos_tokenized_text = word_tokenize(pos_text)

delimiter=['!','@','#','$','%','^','&','*','(',')','_','+','-','=',',','.','<','>','?',';',
           "'",':','"','|','[',']','{','}','/',' ','']
count_del=0
pos_count_del=0
neg_count_del=0
stopset = set(stopwords.words('english'))
filter_stops = lambda w: len(w) < 3 or w in stopset

for i in movie_reviews.fileids():
    words = [w.lower() for w in movie_reviews.words(i)]
    
for i in movie_reviews.words():
        if i in delimiter:
            count_del+=1 #count delimiters and subtract them to the total amount of words

nwords=movie_reviews.words(categories='neg')
pwords=movie_reviews.words(categories='pos')

for i in nwords:
    if i in delimiter:
        neg_count_del+=1

for i in pwords:
    if i in delimiter:
        pos_count_del+=1   
        
total_count = len(pwords)+len(nwords)-count_del
bcf = BigramCollocationFinder.from_words(words)
bcf.apply_word_filter(filter_stops)

print ('The positive files contain:',len(pwords)-pos_count_del,'words.')
print ('The negative files contain:',len(nwords)-neg_count_del,'words.')
print ('The whole storage contains:',total_count,'words.')

print ('The 10 most famous pairs of words are:\n',bcf.nbest(BigramAssocMeasures.likelihood_ratio,10),'\n')
table = str.maketrans('', '', string.punctuation)
pos_stripped = [w.translate(table) for w in pwords]
neg_stripped = [w.translate(table) for w in nwords]

# remove remaining tokens that are not alphabetic and belong to the stopset
pwords = [word for word in pos_stripped if word.isalpha()]
pwords = [w for w in pwords if  w not  in stopset]

nwords = [word for word in neg_stripped if word.isalpha()]
nwords = [w for w in nwords if  w not  in stopset]

#Words' Frequency
pos_freq=FreqDist(pwords)
neg_freq=FreqDist(nwords)

#pos_freq.plot(cumulative=True)
#neg_freq.plot(cumulative=True)

#Convert lists to nltk
n_nltk_words=nltk.Text(nwords)
p_nltk_words=nltk.Text(pwords)
print ('The 10 most popular pairs of words in positive reviews files are:',p_nltk_words.collocations(),10,'\n')
print ('The 10 most popular pairs of words in negative reviews files are:',n_nltk_words.collocations(),10,'\n')

#Split the words in pairs(bigrams)

#convert a list of words into a dict
pbigram=list(bigrams(pwords))
nbigram=list(bigrams(nwords))
def bag_of_words(words):
    return dict([(word, True) for word in words])

#set of words to exclude
def bag_of_words_not_in_set(words, badwords):
    return bag_of_words(set(words) - set(badwords))

#Filtering stopwords
def bag_of_non_stopwords(words, stopfile='english'):
    badwords = stopwords.words(stopfile)
    return bag_of_words_not_in_set(words, badwords)

# =============================================================================
#                                  WEEK 4 
# =============================================================================

# =============================================================================
#                             Simple Tagger        
# =============================================================================
text=word_tokenize(movie_reviews.raw())
text_stripped = [w.translate(table) for w in text]

# remove remaining tokens that are not alphabetic and belong to the stopset
textwords = [word for word in text_stripped if word.isalpha()]
textwords = [w for w in textwords if  w not  in stopset]

tags=nltk.pos_tag(textwords)
tags=TaggedCorpusReader('.', r'.*\.tags',word_tokenizer=SpaceTokenizer(),tagset='en-brown')

tags1=tags.tagged_words()

#Count tags per category
counts = Counter(tag for word,tag in tags1)

text_dict={}
for key,values in tags1:
    text_dict.update({key:values})
    

# =============================================================================
#                             WEEK 5
# =============================================================================
# =============================================================================
#                        Text Classification
# =============================================================================
#create a list of labeled feature sets.
def label_feats_from_corpus(corp, feature_detector=bag_of_words):
    label_feats = collections.defaultdict(list)
    for label in corp.categories():
        for fileid in corp.fileids(categories=[label]):
            feats = feature_detector(corp.words(fileids=[fileid]))
            label_feats[label].append(feats)
    return label_feats

#list of labeled training instances and testing instances
def split_label_feats(lfeats, split=0.6):
    train_feats = []
    test_feats = []
    for label, feats in lfeats.items():
        cutoff = int(len(feats) * split)
        train_feats.extend([(feat, label) for feat in feats[:cutoff]])
        test_feats.extend([(feat, label) for feat in feats[cutoff:]])
    return train_feats, test_feats

lfeats = label_feats_from_corpus(movie_reviews)
train_feats, test_feats = split_label_feats(lfeats, split=0.6)
nb_classifier = NaiveBayesClassifier.train(train_feats)
nb_classifier.show_most_informative_features()

print ('The accuracy of Naive Bayes model  is:',100*nltk.classify.util.accuracy(nb_classifier,test_feats),'%')
print ('The labels that the Naive Bayes classifier produces are:',nb_classifier.labels())

sentence1='This is a sunny day'
sentence2='This is a bad day'

#Predict positive or negative text
outcome1=bag_of_non_stopwords(sentence1)
outcome2=bag_of_non_stopwords(sentence2)
print ('The text belongs to the:',nb_classifier.classify(outcome1),'reviews.\n')
print ('The text belongs to the:',nb_classifier.classify(outcome2),'reviews.\n')

# =============================================================================
#                                POS
# =============================================================================
from nltk.corpus.reader import CategorizedPlaintextCorpusReader
pos_text=word_tokenize(movie_reviews.raw(categories='pos'))
neg_text=word_tokenize(movie_reviews.raw(categories='neg'))
pos_stripped = [w.translate(table) for w in pos_text]
neg_stripped = [w.translate(table) for w in neg_text]

pos_textwords = [word for word in pos_stripped if word.isalpha()]
pos_textwords = [w for w in pos_textwords if  w not  in stopset]
neg_textwords = [word for word in neg_stripped if word.isalpha()]
neg_textwords = [w for w in neg_textwords if  w not  in stopset]

pos_tags=nltk.pos_tag(pos_text)
neg_tags=nltk.pos_tag(neg_text)

pos_reader=TaggedCorpusReader('.', r'.*\.tags',word_tokenizer=SpaceTokenizer(),tagset='en-brown')
neg_reader=TaggedCorpusReader('.', r'.*\.tags',word_tokenizer=SpaceTokenizer(),tagset='en-brown')

POS_pos_lexicon=pos_reader.tagged_words(tagset='universal')
POS_neg_lexicon=neg_reader.tagged_words(tagset='universal')
##
reader=CategorizedPlaintextCorpusReader('C:\\Users\\teo\\Downloads\\movie_reviews',r'(pos|neg)/.*\.txt',cat_pattern=r'(pos|neg)/.*\.txt')
#reader = CategorizedPlaintextCorpusReader('C:/Users/akkii/AppData/Roaming/nltk_data/corpora/movie_reviews', r'(\w+)/*.txt', cat_pattern=r'/(\w+)/.txt')
POS_lfeats = label_feats_from_corpus(reader)
POS_train_feats, POS_test_feats = split_label_feats(POS_lfeats, split=0.6)
POS_nb_classifier = NaiveBayesClassifier.train(POS_train_feats)
POS_nb_classifier.show_most_informative_features()
print ('The accuracy of the test is:',100*nltk.classify.util.accuracy(POS_nb_classifier,POS_test_feats),'%')

pos_dict={}
neg_dict={}
for key,values in POS_pos_lexicon:
    pos_dict.update({key:values})
    
for key,values in POS_neg_lexicon:
    neg_dict.update({key:values})

# =============================================================================
#                 Simple NB Classification
# =============================================================================
documents = [(list(movie_reviews.words(fileid)), category)
              for category in movie_reviews.categories()
              for fileid in movie_reviews.fileids(category)]
all_words = pos_freq+neg_freq
word_features = list(all_words)[:2000]

def document_features(document): 
    document_words = set(document) 
    features = {}
    for word in word_features:
        features['contains({})'.format(word)] = (word in document_words)
    return features

featuresets = [(document_features(d), c) for (d,c) in documents]
Simple_train_set, Simple_test_set = featuresets[100:], featuresets[:100]
Simple_classifier = nltk.NaiveBayesClassifier.train(Simple_train_set)

print('The model is accurate:',100*nltk.classify.accuracy(Simple_classifier, Simple_test_set),'%\n')
Simple_classifier.show_most_informative_features()

# =============================================================================
#                      NB Classification with dev_test
# =============================================================================
dev_train_set, dev_test_set,dev_test = featuresets[500:], featuresets[:500],featuresets[500:1500]
dev_classifier = nltk.NaiveBayesClassifier.train(dev_train_set)
print('The DevTest model is accurate:',100*nltk.classify.accuracy(dev_classifier, dev_test),'%\n')

dev_classifier.show_most_informative_features()

errors = []
for (name, tag) in dev_test:
     guess = dev_classifier.classify(name)
     if guess != tag:
         errors.append( (tag, guess, name) )

def tag_list(tagged_sents):
     return [tag for sent in tagged_sents for (word, tag) in sent]
 
def apply_tagger(tagger, corpus):
     return [tagger.tag(nltk.tag.untag(sent)) for sent in corpus]

tags_tagged_sents = tags.tagged_sents(tagset='universal')
size = int(len(tags_tagged_sents) * 0.6)
test_sents = tags_tagged_sents[size:]
train_sents = tags_tagged_sents[:size]

t0 = nltk.DefaultTagger('NN')
t1 = nltk.UnigramTagger(train_sents, backoff=t0)
t2 = nltk.BigramTagger(train_sents, backoff=t1)

test_tags = [tag for sent in tags.sents() for (word, tag) in t2.tag(sent)]
gold_tags = [tag for (word, tag) in tags.tagged_words(tagset='universal')]
cm = nltk.ConfusionMatrix(gold_tags, test_tags)

print ('The evaluation result is:',100*t2.evaluate(test_sents),'%.\n')
print(cm.pretty_format(sort_by_count=True, show_percents=True, truncate=9))

labels = set('ADJ ADP ADV CONJ DET NOUN NUM PRT VERB X'.split())

true_positives = Counter()
false_negatives = Counter()
false_positives = Counter()

for i in labels:
    for j in labels:
        if i == j:
            true_positives[i] += cm[i,j]
        else:
            false_negatives[i] += cm[i,j]
            false_positives[j] += cm[i,j]

print ("True Positives:", len(list(true_positives.elements())),'\n', true_positives,'\n')
print ("False Negatives:", len(list(false_negatives.elements())),'\n', false_negatives,'\n')
print ("False Positives:", len(list(false_positives.elements())),'\n', false_positives,'\n')




#Calculate precision, recall, f-score
for i in sorted(labels):
    if true_positives[i] == 0:
        fscore = 0
    else:
        precision = true_positives[i] / float(true_positives[i]+false_positives[i])
        recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
        fscore = 2 * (precision * recall) / float(precision + recall)
    print ('***********',i,'label***********')
    print ('The precision measure for the',i,'is:',precision)
    print ('The recall measure for the',i,'is:',recall)
    print ('The f-score for the',i,'is:',fscore,'\n')


cv_train = cross_validation.KFold(len(dev_train_set), n_folds=8, shuffle=False, random_state=None)

for traincv, testcv in cv_train:
    print ('dev-accuracy:', nltk.classify.util.accuracy(Simple_classifier, Simple_train_set[testcv[0]:testcv[len(testcv)-1]]))

cv_test = cross_validation.KFold(len(Simple_test_set), n_folds=8, shuffle=False, random_state=None)

folds=1
kf=['fold0','fold1','fold2','fold3','fold4','fold5','fold6','fold7']
train=['train0','train1','train2','train3','train4','train5','train6','train7']

i=8
while i<len(kf):
    kf[i]=tags_tagged_sents[(i)*(round(len(tags_tagged_sents)/8)):(i+1)*(round(len(tags_tagged_sents)/folds))]
    i+=1
    
train[0]=[kf[1],kf[2],kf[3],kf[4],kf[5],kf[6],kf[7]]
train[1]=[kf[0],kf[2],kf[3],kf[4],kf[5],kf[6],kf[7]]
train[2]=[kf[0],kf[1],kf[3],kf[4],kf[5],kf[6],kf[7]]
train[3]=[kf[0],kf[1],kf[2],kf[4],kf[5],kf[6],kf[7]]
train[4]=[kf[0],kf[1],kf[2],kf[3],kf[5],kf[6],kf[7]]
train[5]=[kf[0],kf[1],kf[2],kf[3],kf[4],kf[6],kf[7]]
train[6]=[kf[0],kf[1],kf[2],kf[3],kf[4],kf[5],kf[7]]
train[7]=[kf[0],kf[1],kf[2],kf[3],kf[4],kf[5],kf[6]]
  
size=['size0','size1','size2','size3','size4','size5','size6','size7']
test=['test0','test1','test2','test3','test4','test5','test6','test7']
t11=['t00','t01','t02','t03','t04','t05','t06','t07']
t21=['t20','t21','t22','t23','t24','t25','t26','t27']
test_tags01=['test_tags0','test_tags1','test_tags2','test_tags3','test_tags4',
           'test_tags5','test_tags6','test_tags7']

gold_tags01=['gold_tags0','gold_tags1','gold_tags2','gold_tags3','gold_tags4',
             'gold_tags5','gold_tags6','gold_tags7']

cm01=['cm0','cm1','cm2','cm3','cm4','cm5','cm6','cm7']
true_positives01=Counter()
false_negatives01=Counter()
false_positives01=Counter()
i=0
while i < folds-1:
    size[i] = int(len(tags_tagged_sents)*((i+1)/folds))
    test[i] = tags_tagged_sents[size[i]:]
    train[i] = tags_tagged_sents[:size[i]]
    t11[i] = nltk.UnigramTagger(train[i], backoff=t0)
    t21[i] = nltk.BigramTagger(train[i], backoff=t11[i])
    print ('The evaluation result is:',100*t21[i].evaluate(test[i]),'%.\n')
    
    test_tags01[i] = [tag for sent in tags.sents() for (word, tag) in t21[i].tag(sent)]
    gold_tags01[i] = [tag for (word, tag) in tags.tagged_words(tagset='universal')]
    cm01[i] = nltk.ConfusionMatrix(gold_tags01[i], test_tags01[i])
    print(cm01[i].pretty_format(sort_by_count=True, show_percents=True, truncate=9))
    
    for k in labels:
        for j in labels:
            if k == j:
                true_positives01[k] += cm[k,j]
            else:
                false_negatives01[k] += cm[k,j]
                false_positives01[j] += cm[k,j]
    print ("True Positives:", len(list(true_positives01.elements())),'\n', true_positives01,'\n')
    print ("False Negatives:", len(list(false_negatives01.elements())),'\n', false_negatives01,'\n')
    print ("False Positives:", len(list(false_positives01.elements())),'\n', false_positives01,'\n')
    
#Calculate precision, recall, f-score
    for j in sorted(labels):
        if true_positives01[j] == 0:
            fscore = 0
        else:
            precision = true_positives01[j] / float(true_positives01[j]+false_positives01[j])
            recall = true_positives01[j] / float(true_positives01[j]+false_negatives01[j])
            fscore = 2 * (precision * recall) / float(precision + recall)
        print ('***********',j,'label***********')
        print ('The precision measure for the',j,'is:',precision)
        print ('The recall measure for the',j,'is:',recall)
        print ('The f-score for the',j,'is:',fscore,'\n')
    i+=1

# =============================================================================
#                            WEEK 6
# =============================================================================
# =============================================================================
#                           Perplexity
# =============================================================================
def unigram(tokens):    
    model = collections.defaultdict(lambda: 0.01)
    for f in tokens:
        try:
            model[f] += 1
        except KeyError:
            model [f] = 1
            continue
    for word in model:
        model[word] = model[word]/float(sum(model.values()))
    return model

def perplexity(testset, model):
    testset = testset.split()
    perplexity = 1
    N = 0
    for word in testset:
        N += 1
        perplexity = perplexity * (1/model[word])
    perplexity = pow(perplexity, 1/float(N)) 
    return perplexity

model = unigram(text)
testset1="This is a test sentence"
print("The perplexityi is:")
print (perplexity(testset1, model))

# =============================================================================
#                               WEEK 10
# =============================================================================
sentences = [" ".join(i) for i in movie_reviews.sents() if i not in delimiter]
random_sentences = [sentences[random.randrange(len(sentences))] for i in range(20)]
tagged_words = [ ]
all_tags = [ ]

for i in pos_tag_sents(movie_reviews.sents(),tagset='universal'): # get tagged sentences
    tagged_words.append( ("START", "START") )
    all_tags.append("START")
    for (word, tag) in i:
        all_tags.append(tag)
        tagged_words.append( (tag, word) ) 
    tagged_words.append( ("END", "END") )
    all_tags.append("END")

tags_cfd= nltk.ConditionalFreqDist(nltk.bigrams(all_tags))
tags_cpd = nltk.ConditionalProbDist(tags_cfd, nltk.MLEProbDist)
tagged_words_cfd = nltk.ConditionalFreqDist(tagged_words)
tagged_words_cpd = nltk.ConditionalProbDist(tagged_words_cfd, nltk.MLEProbDist)


def viterbi(sentence):
    # Step 1. initialization step 
    distinct_tags = np.array(list(set(all_tags)))
    tagslen = len(distinct_tags)
    sentlen = len(sentence)
    viterbi = np.zeros((tagslen, sentlen+1) ,dtype=float)
    backpointer = np.zeros((tagslen, sentlen+1) ,dtype=np.uint32) 
   
    # Step 1. initialization step
    for s, tag in enumerate(distinct_tags):
        viterbi[s,0] =  tags_cpd["START"].prob(tag) * tagged_words_cpd[tag].prob( sentence[0] )
        backpointer[s,0] = 0
    
    # Step 2. recursion step
    for t in range(1, sentlen):
        for s, tag in enumerate(distinct_tags):
            current_viterbi = np.zeros( tagslen ,dtype=float)
            for sprime, predtag in enumerate(distinct_tags):
                current_viterbi[sprime] = viterbi[sprime,t-1] * \
                                          tags_cpd[predtag].prob(tag) * \
                                          tagged_words_cpd[tag].prob(sentence[t])
            backpointer[s,t] = np.argmax(current_viterbi)
            viterbi[s,t] = max(current_viterbi)
    
    # Step 3. termination step
    current_viterbi = np.empty( tagslen ,dtype=float)
    ind_of_end = -1
    for s, tag in enumerate(distinct_tags):
        if tag == "END":
            ind_of_end  = s
        current_viterbi[s] = viterbi[s,sentlen-1] * tags_cpd[tag].prob("END") 
           
    backpointer[ind_of_end,sentlen] = np.argmax(current_viterbi)
    viterbi[ind_of_end,sentlen] = max(current_viterbi)
            
    # Step 3. backtrace the path
    best_tagsequence = [ ]
    prob_tagsequence = viterbi[ind_of_end,sentlen]
    prevind  = ind_of_end
    for t in range(sentlen,0,-1):
        prevind = backpointer[prevind,t]
        best_tagsequence.append(distinct_tags[prevind])
    best_tagsequence.reverse()
    return best_tagsequence, prob_tagsequence
probs=[]
sentence=[]
i=0 
while i<len(random_sentences):
    sentence.append(nltk.word_tokenize(random_sentences[i]))
    best_tagsequence,prob_tagsequence = viterbi(sentence[i])    
    probs.append(prob_tagsequence)
    i+=1
    
for w,t,z in zip(random_sentences,best_tagsequence,probs): 
    print ("The best tag sequence for the sentence:\n",w,'\n is:',t,'with probability:',z,'\n')   
0import pandas as pd
from textblob.classifiers import NaiveBayesClassifier
import string
import nltk
from nltk import word_tokenize
from nltk import bigrams
from nltk.collocations import BigramCollocationFinder
from nltk.corpus import stopwords
from nltk.metrics import BigramAssocMeasures
import collections
from collections import Counter
from nltk.tag import pos_tag_sents

data=pd.read_csv(r'F:\DEREE\NLP\Project\Book2.csv',delimiter=';')
data['reviews.text']=data['reviews.text'].str.lower()
data['reviews.rating']=round(data['reviews.rating'])

#remove punctuation
data['reviews.text']=data['reviews.text'].astype(str)
data['reviews.text'] = data['reviews.text'].apply(lambda x:''.join([i for i in x 
                            if i not in string.punctuation]))
#remove stopwords
data['reviews.text'] = [word for word in data['reviews.text'] if word not in stopwords.words('english')]

#remove numbers
data['reviews.text']=data['reviews.text'].str.replace('\d+', '')

data_list=[data]

for dataset in data_list:
    print("+++++++++++++++++++++++++++")
    print(pd.isnull(dataset).sum() >0)
    print("+++++++++++++++++++++++++++")

if pd.isnull(data['reviews.rating']).sum().all()==0:
    print ('There are no missing values')
else:
    print ('There are missing values')
    
#convert floats to int
data['reviews.rating']=data['reviews.rating'].astype(int)
i=0
while i < len(data): 
    if data['reviews.rating'][i]>=8:
        data['reviews.rating'][i]='perfect'
    elif ((data['reviews.rating'][i]>=6) & (data['reviews.rating'][i] <= 7)):
        data['reviews.rating'][i]='good'
    elif ((data['reviews.rating'][i]>=4) & (data['reviews.rating'][i] <= 5)):
        data['reviews.rating'][i]='satisfactory'
    else:
        data['reviews.rating'][i] = 'not good'
    i+=1

# =============================================================================
#                   Naive Bayes Classification        
# =============================================================================
total= []
data['text']=data['reviews.text']
data['rating']=data['reviews.rating']
for j in  zip(data.text,data.rating):
        total.append((j))
        
train = total[:1000]
test = total[1000:2000]









cl = NaiveBayesClassifier(train)

# Compute accuracy
print("Accuracy: {0}".format(100*cl.accuracy(test)),'%')

# Show  most informative features
cl.show_informative_features(15)

#Remove the '' from the set
u=0
while u< 4:
    for i in total:
        for j in i:
            if j == '':
                total.remove(i)
    u+=1
            
total_words=nltk.Text(total)

for k,v in total_words:
    if k == '':
        print (v)

bigram_measures = nltk.collocations.BigramAssocMeasures()
tokens=()
for i in data['reviews.text']:
    tokens=tokens+word_tokenize(i)

finder = BigramCollocationFinder.from_words(tokens)
finder.nbest(bigram_measures.pmi, 10)

print ('The 10 most popular pairs of words are:\n',finder.nbest(bigram_measures.pmi, 10),'\n')

stopset = set(stopwords.words('english'))
filter_stops = lambda w: len(w) < 3 or w in stopset
finder.apply_word_filter(filter_stops)
print ('The most famous pairs are:',finder.nbest(BigramAssocMeasures.likelihood_ratio, len(total_words)))

total_bigrams=list(bigrams(total_words))

# =============================================================================
#                                 Perplexity
# =============================================================================
def unigram(tokens):    
    model = collections.defaultdict(lambda: 0.01)
    for f in tokens:
        try:
            model[f] += 1
        except KeyError:
            model [f] = 1
            continue
    for word in model:
        model[word] = model[word]/float(sum(model.values()))
    return model

def perplexity(testset, model):
    testset = testset.split()
    perplexity = 1
    N = 0
    for word in testset:
        N += 1
        perplexity = perplexity * (1/model[word])
    perplexity = pow(perplexity, 1/float(N)) 
    return perplexity

model = unigram(total_words)
testset1="This is a test sentence"
print (perplexity(testset1, model))

# =============================================================================
#                              DEVTEST
# =============================================================================
documents = [(list(total_words), category)
              for category in data['reviews.rating'][:100]]

word_features = list(total_words)[:]

def reviews(review):
    features = {}
    features["first_letter"] = review[0].lower()
    features["last_letter"] = review[-1].lower()
    for letter in 'abcdefghijklmnopqrstuvwxyz':
        features["count({})".format(letter)] = total_words.count(letter)
        features["has({})".format(letter)] = (letter in total_words)
    return features

featuresets = [(reviews(i), j) for (i, j) in total_words[:100]]
train_set = total_words[:round(len(data)/3)]
test_set = total_words[round(len(data)/3):2*round(len(data)/3)]
dev_test = total_words[2*round(len(data)/3):]

train1_set = [(reviews(n), j) for (n, j) in train_set[:100]]
dev1_test = [(reviews(n), j) for (n, j) in dev_test[:100]]
test1_set = [(reviews(n), j) for (n, j) in test_set[:100]]
dev_classifier = nltk.NaiveBayesClassifier.train(test1_set)

dev_classifier.show_most_informative_features()
print('The model is accurate:',100*nltk.classify.accuracy(dev_classifier, dev1_test),'%\n')

errors = []
for (name, tag) in dev1_test:
     guess = dev_classifier.classify(name)
     if guess != tag:
         errors.append( (tag, guess, name) )

def tag_list(tagged_sents):
     return [tag for sent in tagged_sents for (word, tag) in sent]
 
def apply_tagger(tagger, corpus):
     return [tagger.tag(nltk.tag.untag(sent)) for sent in corpus]

#Store the reviews as a string 
text = [str(y) for y in data['reviews.text']] 
#Remove the '' from the set
u=0
while u< 4:
    for i in text:
        if i == '':
                text.remove(i)
    u+=1

tags=nltk.pos_tag(text)
tags_tagged_sents = pos_tag_sents(tags,tagset='universal')
size = int(len(tags_tagged_sents) * 0.7)
train_sents = tags_tagged_sents[:size]
test_sents = tags_tagged_sents[size:]

t0 = nltk.DefaultTagger('NN')
t1 = nltk.UnigramTagger(train_sents, backoff=t0)
t2 = nltk.BigramTagger(train_sents, backoff=t1)
gold = tag_list(pos_tag_sents(tags,tagset='universal'))
test1 = tag_list(apply_tagger(t2, pos_tag_sents(tags,tagset='universal')))
cm = nltk.ConfusionMatrix(gold, test1)

print ('The evaluation result is:',100*t2.evaluate(test_sents),'%.\n')
print(cm.pretty_format(sort_by_count=True, show_percents=True, truncate=8))

labels = set('ADJ ADP ADV DET NOUN VERB NUM CONJ'.split())

true_positives = Counter()
false_negatives = Counter()
false_positives = Counter()

for i in labels:
    for j in labels:
        if i == j:
            true_positives[i] += cm[i,j]
        else:
            false_negatives[i] += cm[i,j]
            false_positives[j] += cm[i,j]

print ("True Positives:", len(list(true_positives.elements())),'\n', true_positives,'\n')
print ("False Negatives:", len(list(false_negatives.elements())),'\n', false_negatives,'\n')
print ("False Positives:", len(list(false_positives.elements())),'\n', false_positives,'\n')

#Calculate precision, recall, f-score
for i in sorted(labels):
    if true_positives[i] == 0:
        fscore = 0
    else:
        precision = true_positives[i] / float(true_positives[i]+false_positives[i])
        recall = true_positives[i] / float(true_positives[i]+false_negatives[i])
        fscore = 2 * (precision * recall) / float(precision + recall)
    print ('***********',i,'label***********')
    print ('The precision measure for the',i,'is:',precision)
    print ('The recall measure for the',i,'is:',recall)
    print ('The f-score for the',i,'is:',fscore,'\n')
# =============================================================================
#                      Decision Trees Classification
# =============================================================================
dt_classifier = nltk.classify.DecisionTreeClassifier.train(dev1_test, entropy_cutoff=0,support_cutoff=0)
print('The model is accurate:',100*nltk.classify.accuracy(dt_classifier, dev1_test),'%\n')
#
#
#import numpy as np
#from sklearn.neural_network import MLPClassifier
#from sklearn.model_selection import train_test_split
#from sklearn.metrics import classification_report,confusion_matrix
#from nltk.stem.lancaster import LancasterStemmer
#from sklearn.metrics import f1_score
#from sklearn.metrics import precision_recall_fscore_support
##
#training_data=[]
#words1=[]
#documents=[]
#classes=[]
#stemmer = LancasterStemmer()
#training = []
#output = []
#bag = []
#
#for i in tags:
#    for j in i:
#        training_data.append({'class':i,'sentence':j})
#for pattern in training_data[:10]:    
#    # tokenize each word in the sentence
#    w = nltk.word_tokenize(pattern['sentence'])
#    # add to our words list
#    words1.extend(w)
#    # add to documents in our corpus
#    documents.append((w, pattern['class']))
#    # add to our classes list
#    if pattern['class'] not in classes:
#          classes.append(pattern['class'])
#    # stem and lower each word and remove duplicates
#    words1 = [stemmer.stem(w.lower()) for w in words1]
#    # create our training data
## create an empty array for our output
#output_empty = [0] * len(classes)
## training set, bag of words for each sentence
#for doc in documents:
#    # initialize our bag of words
#    # list of tokenized words for the pattern
#    pattern_words = doc[0]
#    # stem each word
#    pattern_words = [stemmer.stem(word.lower()) for word in pattern_words]
#    # create our bag of words array
#    for w in total_words:
#        bag.append(1) if w in pattern_words else bag.append(0)
#    training.append(bag)
#    # output is a '0' for each tag and '1' for current tag
#    output_row = list(output_empty)
#    output_row[classes.index(doc[1])] = 1
#    output.append(output_row)        
#
#i = 0
#w = documents[i][0]
#print ([stemmer.stem(word.lower()) for word in w])
#print (training[i])
#print (output[i])
#
#X = np.array(training)
#y = np.array(output)
#y = np.ravel(y,order="C")
#
#X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
#
#clfANN=MLPClassifier(hidden_layer_sizes=20,alpha=0.1)
#clfANN.fit(X_train,y_train)
#clfANN_train_predictions=clfANN.predict(X_train)  
#clfANN_test_predictions=clfANN.predict(X_test)  
#clfANN_report=classification_report(y_test,clfANN_test_predictions)
#clfANN_conf_matrix=confusion_matrix(y_test,clfANN_test_predictions)
#
#print ('-------------------------ANN method--------------------------')
#print ('The confusion matrix for ANN method is\n',clfANN_conf_matrix,'\n')
#print ('===>>> Macro')
#print ('Train f1 for ANN method is=',format(f1_score(y_train, clfANN_train_predictions, average='macro'),'.4f'),'\n')
#print ('Test f1 for ANN method is=',format(f1_score(y_test, clfANN_test_predictions, average='macro'),'.4f'),'\n')
#print ('Train precision for ANN method is=',precision_recall_fscore_support(y_train, clfANN_train_predictions, average='macro'),'\n')
#print ('Test precision for ANN method is=',precision_recall_fscore_support(y_test, clfANN_test_predictions, average='macro'),'\n')
#print ('===>>> Micro')
#print ('Train f1 for ANN method is=',format(f1_score(y_train, clfANN_train_predictions, average='micro'),'.4f'),'\n')
#print ('Test f1 for ANN method is=',format(f1_score(y_test, clfANN_test_predictions, average='micro'),'.4f'),'\n')
#print ('Train precision for ANN method is=',precision_recall_fscore_support(y_train, clfANN_train_predictions, average='micro'),'\n')
#print ('Test precision for',i,'method is=',precision_recall_fscore_support(y_test, clfANN_test_predictions, average='micro'),'\n')
#print ('===>>> None')
#print ('Train f1 for ANN method is=',f1_score(y_train, clfANN_train_predictions, average=None),'\n')
#print ('Test f1 for ANN method is=',f1_score(y_test, clfANN_test_predictions, average=None),'\n')
#print ('Train precision for ANN method is=',precision_recall_fscore_support(y_train, clfANN_train_predictions, average=None),'\n')
#print ('Test precision for ANN method is=',precision_recall_fscore_support(y_test, clfANN_test_predictions, average=None),'\n')
#print ('Test report for the  ANN method is:','\n',clfANN_report,'\n')